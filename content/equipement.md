---
title: "Où s'équiper"
date: 2017-11-15T14:04:27+01:00
draft: false
private: true
description: "Dans quels magazins acheter son équipement de Iaido"
taxonomies:
  category: equipement
  tag: equipement, équipement, iaito, keigogi, obi, tabi, genouillères
---

### Où s'équiper

#### en France

* https://www.masamune-store.com/

#### au Japon

* http://nosyudo.jp/ (boutique apparement réputée)
* http://www.sakuraya.org/

#### sur Internet

* https://www.tozandoshop.com/

sinon, l'ensemble des boutiques référencées ci-dessus ont un site web marchand.
