---
title: "Les Dojos"
date: 2017-11-15T14:04:27+01:00
draft: false
private: true
---

### Kazuki dojo
![Dojo Shiseikan](/images/salle-dojo-shiseikan1.jpg)
#### Horaires
* dimanche : de 18h à 19h30
* mardi : de 21h à 22h30
#### Adresse
75 rue Lecourbe, 75015 Paris       
Métros : ligne 12 Volontaires ou Ligne 6 Sèvres-Lecourbe
#### Contacts
Téléphones: 01 45 67 51 51 - 06 77 70 77 29 du lundi au samedi
#### Instructeurs officiel 
* M. Dino Perrone 4ème dan / Shidôin
* Mlle Linh Thiers 3ème dan / Shidôin
#### Site web
https://mugairyufrance.jimdo.com/mugai-ryu/
