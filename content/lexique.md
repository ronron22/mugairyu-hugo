---
title: "Lexique"
date: 2017-11-26T20:28:57+01:00
draft: false
private: true
menuname: "Lexique"
---

## Repères

Seichusen

:   la ligne d'attaque

Taisoku

:   repère : lignes partant de l’extérieur de chaque hanche et projetées vers l'avant (perpendiculaire au buste)

Suigetsu

:   le plexus solaire

Semé

:   la menace

Metsuke

:   le regard

___

## Types de frappe
Yoko ichi monji

:   frappe horizontal (effectuée au niveau des seins dans notre école)

Kiri age

:   nom générique pour les frappes **Gyaku** (à confirmer)

Tsuki

:   frappe d'estoc (à confirmer)

Mako

:   frappe vertical (de haut en bas)

Kesa giri

:   frappes de biais de haut en bas, elles entrent à la jonction cou/épaule et ressortent au dessus du bassin <u>et pas au niveau du bassin</u>

Gyaku kesa giri

:   frappes de biais de bas en haut, elles entrent au dessus du bassin  <u>et pas au niveau du bassin</u>  et ressortent à la jonction cou/épaule

Katate soe giri

:   coupe d'une main, la main gauche ne venant saisir la Tuska qu'à la fin
___

## Gardes

Jodan

:   garde haute (au dessus de la tête)

Asso

:   garde haute (tsuba prêt de la bouche)

Seigan

:   garde moyenne, la lame pointant vers la gorge de l'ennemi, *kashira* à au moins deux poings du ventre

Ira seigan

:   garde moyenne, sabre parrallèle au sol, *kashira* à un poing du ventre

Gedan

:   garde basse, la lame est inclinée, la pointe devant la jambe droite, la *tsuka* bien décalée sur la gauche et le kissaki à hauteur de genou (à peu près)

___

## Actions
Saya biki

:   envoi du fourreau d'un geste vif vers l'arrière

Obi te

:   pause de la main gauche sur le Obi, pouce fermé lors du combat puis pousse ouvert pour le Noto

Kiri Te

:   l'angle de coupe du poignet

Shiki giri

:   action de coupe (à confirmer)
