---
title: "L'attitude mental en Iaido"
date: 2018-11-08T20:59:05+01:00
draft: false
private: true
menuname: "mental-attitude"
---

Par **Konichi Misakazu Ryuoh**, 16ème *Soke* du *Mugairyu* 

Pratiquer sans opposant, la tension spirituelle de l'*Iaido*, moins importante que dans d'autres arts martiaux, invite à un relâchement contre lequel le pratiquant doit constament se méfier.  

Cette pratique doit être entreprise en gardant à l'esprit qu'il s'agit d'une confrontation avec un opposant et doit se comprendre dans une situation de vie ou de mort, sans appréciation de cette réalité, l'*Iaido* peut devenir une mascarade, il faut constament être conscient de cela.

Du fait qu'un vrai sabre soit utilisé en *Iaido*, à chaque instant l'esprit doit rester concentré.  
Si l'on ne pratique pas avec son coeur, nos actions souillerons l'esprit incarné dans le sabre.  
L'art martial qu'est le *Iaido* ne doit pas être confondu avec une pratique amateur du sabre.

Les niveaux et titres existent pour encourager les pratiquants, toutefois, ces derniers ne devront pas perdre de vue l'essence de l'*Iaido*.

Une pratique de l'*Iaido* limité à l'acquisition de titre se réduirait alors à une succession de mouvements vides et techniquement vulgaires ;  cette pratique ne doit pas être entreprise.

En combat, le premier combat à gagner est contre soi-même. Il est dit « gagner contre soi, gagner contre ses collègues et enfin, gagner contre ses ennemis ». Dans l'ancien temps, il était connu que gagner contre soi-même était l'esprit du contrôle de soi (self-control).  
Il est important d'intégrer le contrôle de soi au travers de sa pratique de l'*Iaido*.

Extrait du *« Préface du Mugai ryu iaido shisin »*

Note du traducteur  
Ce texte a été traduit de l'anglais, lui-même traduit du japonais.

Analyse du traducteur

Le fait de pratiquer seul tend à rendre abstrait la situation de combat où se situerait normalement le *kata*, il est donc nécessaire de se représenter mentalement et emotionnellement les ennemis et la situation.

Une conscience aïgue de la confrontation maximisera les bénéfices apportés par la pratique de l'Iaido, Le pratiquant en sera alors transformé sinon, le kata se réduira à une succession de mouvements vides de sens n'entraînant aucune transformation « profonde » du pratiquant. 

La pratique avec un *Katana* coupant engendre une vigilance particulière participant activement à l'esprit du iaido et aboutissant à la transformation positive de l'état d'esprit du pratiquant.
Au Japon, en *Mugai ryu*, dès le 3ème dan, certains pratiquants sont encouragés à utiliser un *katana*.
